package repository

import (
    "log"
    _ "github.com/lib/pq"
    "github.com/jmoiron/sqlx"
)

type customerRepositoryDB struct {
    db *sqlx.DB
}

func NewCustomerRepositoryDB(db *sqlx.DB) customerRepositoryDB {
    return customerRepositoryDB{db: db}
}

func (r customerRepositoryDB) InsertCustomer(customer Customer) (int, error) {
    query := "INSERT INTO customers (customer_id, customer_name, phone_number, date_created) VALUES (DEFAULT, $1, $2, now()) RETURNING customer_id"
    var lastInsertID int
    err := r.db.QueryRow(query, customer.CustomerName, customer.PhoneNumber).Scan(&lastInsertID)
    if err != nil {
        log.Printf("Error inserting customer: %v", err)
        return 0, err
    }
    log.Printf("Inserted customer with ID %d", lastInsertID)
    return int(lastInsertID), nil
}


func (r customerRepositoryDB) RemoveCustomer(customerID int) error {
    query := "DELETE FROM customers WHERE customer_id = $1"
    _, err := r.db.Exec(query, customerID)
    if err != nil {
        log.Printf("Error removing customer with ID %d: %v", customerID, err)
        return err
    }
    log.Printf("Removed customer with ID %d", customerID)
    return nil
}

func (r customerRepositoryDB) UpdateCustomer(customer Customer) (int, error) {
    query := "UPDATE customers SET customer_name = $1, phone_number = $2 WHERE customer_id = $3"
    result, err := r.db.Exec(query, customer.CustomerName, customer.PhoneNumber, customer.CustomerID)
    if err != nil {
        log.Printf("Error updating customer with ID %d: %v", customer.CustomerID, err)
        return 0, err
    }
    rowsAffected, err := result.RowsAffected()
    if err != nil {
        log.Printf("Error getting rows affected: %v", err)
        return 0, err
    }
    log.Printf("Updated %d customers with ID %d", rowsAffected, customer.CustomerID)
    return int(rowsAffected), nil
}

func (r customerRepositoryDB) GetAll() ([]Customer, error) {
    customers := []Customer{}
    query := "SELECT customer_id, customer_name, phone_number, date_created FROM customers"
    err := r.db.Select(&customers, query)
    if err != nil {
        log.Printf("Error getting all customers: %v", err)
        return nil, err
    }
    log.Printf("Retrieved all customers (%d total)", len(customers))
    return customers, nil
}

func (r customerRepositoryDB) GetById(id int) (*Customer, error) {
    customer := Customer{}
    query := "SELECT customer_id, customer_name, phone_number, date_created FROM customers WHERE customer_id = $1"
    err := r.db.Get(&customer, query, id)
    if err != nil {
        log.Printf("Error getting customer with ID %d: %v", id, err)
        return nil, err
    }
    log.Printf("Retrieved customer with ID %d", id)
    return &customer,err
}