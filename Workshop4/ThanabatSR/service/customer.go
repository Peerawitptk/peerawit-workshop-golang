package service

type CustomerResponse struct {
	CustomerID   int    `json:"customer_id"`
	CustomerName string `json:"customer_name"`
	PhoneNumber  string `json:"phone_number"`
	DateCreated  string `json:"date_created"`
}
type CustomerService interface {
	GetCustomers() ([]CustomerResponse, error)
	GetCustomer(int) (*CustomerResponse, error)
	InsertCustomer(CustomerResponse) (int, error)
	RemoveCustomer(int) error
	UpdateCustomer(int, CustomerResponse) (int, error)
}
