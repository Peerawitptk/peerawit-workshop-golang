package service

import (
	"database/sql"
	"hexa_arh/errs"
	"hexa_arh/logs"
	"hexa_arh/repository"
	"net/http"
)

type customerService struct {
	customerServiceRepository repository.CustomerRepository
}

func NewCustomerService(customerServiceRepository repository.CustomerRepository) customerService {
	return customerService{customerServiceRepository: customerServiceRepository}
}

func (s customerService) GETCustomers() ([]CustomerResponse, error) {
	customers, err := s.customerServiceRepository.GetAll()
	if err != nil {
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	customerResponses := []CustomerResponse{}
	for _, customer := range customers {
		customerResponse := CustomerResponse{
			CustomerId:   customer.CustomerId,
			CustomerName: customer.CustomerName,
			Phone_number: customer.Phone_number,
			Date_created: customer.Date_created,
		}
		customerResponses = append(customerResponses, customerResponse)
	}
	return customerResponses, nil

}

func (s customerService) GETCustomer(id int) (*CustomerResponse, error) {
	customer, err := s.customerServiceRepository.GetById(id)
	if err != nil {
		logs.Error(err)
		if err == sql.ErrNoRows {
			return nil, errs.AppError{
				Code:    http.StatusNotFound,
				Message: err.Error(),
			}
		}
		return nil, errs.AppError{
			Code:    http.StatusBadGateway,
			Message: err.Error(),
		}
	}

	customerResponse := &CustomerResponse{
		CustomerId:   customer.CustomerId,
		CustomerName: customer.CustomerName,
		Phone_number: customer.Phone_number,
		Date_created: customer.Date_created,
	}

	return customerResponse, nil
}

func (s customerService) ADDCustomer(c repository.Customer) (int, error) {
	customerid, err := s.customerServiceRepository.AddCustomer(c)
	if err != nil {
		logs.Error(err)
		return 0, errs.AppError{
			Code:    http.StatusConflict,
			Message: err.Error(),
		}
	}
	return customerid, nil
}

func (s customerService) DELETECustomer(id int) error {
	err := s.customerServiceRepository.DeleteCustomer(id)
	if err != nil {
		logs.Error(err)
		return errs.AppError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		}
	}
	return nil
}

func (s customerService) UPDATECustomer(c repository.Customer, id int) (int, error) {
	customerId, err := s.customerServiceRepository.UpdateCustomer(c, id)
	if err != nil {
		logs.Error(err)
		return 0, errs.AppError{
			Code:    http.StatusNotFound,
			Message: err.Error(),
		}
	}
	return customerId, nil
}
